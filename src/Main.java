import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by ahmedrizwan on 05/01/2016.
 */
public class Main {

    static int depth = 0;
    static int maxLength = 0;
    static int maxSteep = 0;
    static int temp = -1;
    static int size = -1;
    static List<Integer> values = new ArrayList<Integer>();

    public static void main(String[] args) {
        try {
            Path path = FileSystems.getDefault().getPath("DataSource.txt").toAbsolutePath();
            System.out.println(path);
            Files.lines(path).forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    final String[] vals = s.split(" ");
                    if (vals.length == 2)
                        size = Integer.parseInt(vals[0]);
                    else
                        for (String s1 : vals) {
                            try {
                                values.add(Integer.parseInt(s1));
                            }catch (Exception e){

                            }
                        }
                }
            });
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        int arraySize = values.size();
        System.out.println("The size of array is " + arraySize + " and size per dimension " + Main.size);
        for (int i = 0; i < arraySize; i++) {
            depth = 0;
            temp = values.get(i);
            length(i);
        }

        System.out.println("The max length is " + maxLength + " and steep is " + maxSteep);
    }

    public static void length(final int index) {
        depth += 1;

        final boolean rightIsLess = (index + 1) % size != 0 && values.get(index) > values.get(index + 1);
        final boolean leftIsLess = index % size != 0 && values.get(index) > values.get(index - 1);
        final boolean belowIsLess = index + size < size * size && values.get(index) > values.get(index + size);
        final boolean aboveIsLess = index - size >= 0 && values.get(index) > values.get(index - size);

        if (rightIsLess) {
            length(index + 1);
        }

        if (leftIsLess) {
            length(index - 1);
        }

        if (belowIsLess) {
            length(index + size);
        }

        if (aboveIsLess) {
            length(index - size);
        }

        //if at the leaf node, check the length, assign it to max length and reset
        if (!rightIsLess || !leftIsLess || !belowIsLess || !aboveIsLess) {
            if (depth > maxLength) {
                maxLength = depth;
            }

            if (temp - values.get(index) > maxSteep)
                maxSteep = temp - values.get(index);
        }

        depth--;
    }

}
